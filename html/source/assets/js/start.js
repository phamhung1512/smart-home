(function( $ ) {
/**
 * START - ONLOAD - JS
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */



/* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */
$(document).ready(function($){
    $( "#slider-range-min" ).slider({
        range: "min",
        value: 37,
        min: 0,
        max: 100,
        slide: function( event, ui ) {
          $( "#slider-range-min .ui-slider-handle" ).text(ui.value);
        }
    });

    $("#slider-range-min .ui-slider-handle" ).text($( "#slider-range-min" ).slider( "value" ));

    $( "#tv-range" ).slider({
        range: "min",
        value: 10,
        min: 0,
        max: 100,
        slide: function( event, ui ) {
          $( "#tv-range .ui-slider-handle" ).text(ui.value);
        }
    });

    $("#tv-range .ui-slider-handle" ).text($( "#tv-range" ).slider( "value" ));
    
    $( "#audio-control" ).slider({
        range: "min",
        value: 10,
        min: 0,
        max: 100,
        slide: function( event, ui ) {
          $( "#audio-control .ui-slider-handle" ).text(ui.value);
        }
    });

    $("#audio-control .ui-slider-handle" ).text($( "#audio-control" ).slider( "value" ));
    
    $( "#light-control" ).slider({
        range: "min",
        value: 10,
        min: 0,
        max: 100,
        slide: function( event, ui ) {
          $( "#light-control .ui-slider-handle" ).text(ui.value);
        }
    });

    $("#light-control .ui-slider-handle" ).text($( "#light-control" ).slider( "value" ));
    
    $( "#refrigerator-control" ).slider({
        range: "min",
        value: 10,
        min: 0,
        max: 100,
        slide: function( event, ui ) {
          $( "#refrigerator-control .ui-slider-handle" ).text(ui.value);
        }
    });

    $("#refrigerator-control .ui-slider-handle" ).text($( "#refrigerator-control" ).slider( "value" ));
});
/* OnLoad Window */
var init = function () {

};
window.onload = init;

})(jQuery);
